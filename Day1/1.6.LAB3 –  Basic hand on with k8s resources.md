
# 1.Deploy a nginx pod to k8s
- create pod
kubectl run my-nginx --image=nginx  --port=80

- explore pod
kubectl get pods                              
kubectl get pod [pod-name] -o yaml               
kubectl describe pods [pod-name]
kubectl get pods --show-labels

- Acess to service
kubectl port-forward [pod-name] 80:80

# 2.Deploy the application in LAB1#2 to a custom namespace
- Create a namespace
kubectl create ns web-server
=====
- register dockerhub.com 
- Login to dockethub
- Create a repos
==
# on local computer
Docker build . -t [your-account]/[your-image-name]:[tag-v1]
docker login
docker push [your-account]/[your-image-name]:[tag-v1]
- Create a pod
kubectl run my-service --image=[your-image-name]:[tag-v1] --port=80
- Acess to service
kubectl port-forward my-service 80:80

# 3.Deploy a nginx deployment to k8s
kubectl create deployment [deployment-name] --image=nginx

kubectl get deployments
kubectl describe deployment [deployment-name]
- expose service
kubectl expose deployment nginx --type=LoadBalancer --port=80 
kubectl get services
# Deplopy the application in LAB1#2 to k8s and update it with near-zero downtime
kubectl create deployment [deployment-name] --image=[your-account]/[your-image-name]:[tag-v1]
kubectl get deploy -o wide
kubectl describe deployment [deployment-name]
- Build your v2 service 
docker build . -t [your-account]/[your-image-name]:[tag-v2]
docker push [your-account]/[your-image-name]:[tag-v2]
- rollout to v2
kubectl set image deployment/[deployment-name] [container-name]=[your-image-name]:[tag-v2]
kubectl rollout status deployment/[deployment-name]



