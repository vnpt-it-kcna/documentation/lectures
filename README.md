# Courses outlines
Init sessions
## Day 1:
(Optional) Containerized fundamentals
LAB1 - Using Docker in practical 
Kubernetes - fundamentals
LAB2 - kubernetes in local development.md
Kubernetes - resources
LAB3 –  Basic hand on with k8s resources
## Day 2:
Kubernetes - Scheduling
Kubernetes - networking
Kubernetes - Storage
LAB4 - Scheduling, networking and storage.md
Kubernetes - service mesh
## Day 3:
Cloud native - Architecture
Cloud native - Observability
LAB5 - Monitoring with Prometheus and Grafana
LAB6 - Logging with EFK
Cloud native - Application delivery
LAB7 -  GitOps with Gitlab
## Day 4:
The Capstone project